import React from 'react';
import Loadable from 'react-loadable';
import { PageLoader } from 'meteor/meronex:components';

const loading = () => (
  <div className="loading">
    <PageLoader />
  </div>
);

const AsyncEvents = Loadable({
  loader: () => import('./Events.js').then(ex => ex.default),
  loading
});

export default AsyncEvents;
