import React from 'react';
import Loadable from 'react-loadable';
import { PageLoader } from 'meteor/meronex:components';

const loading = () => (
  <div className="loading">
    <PageLoader />
  </div>
);

const AsyncNews = Loadable({
  loader: () => import('./News.js').then(ex => ex.default),
  loading
});

export default AsyncNews;
