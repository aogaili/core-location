import React from 'react';
import Loadable from 'react-loadable';

const loading = () => (
  <div className="loading">
    <span className="fa fa-refresh fa-spin" />
  </div>
);

const AsyncPhotos = Loadable({
  loader: () => import('./Photos.js').then(ex => ex.default),
  loading
});

export default AsyncPhotos;
