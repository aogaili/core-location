import React from 'react';
import Loadable from 'react-loadable';
import { PageLoader } from 'meteor/meronex:components';

const loading = () => (
  <div className="loading">
    <PageLoader />
  </div>
);

const AsyncPlaces = Loadable({
  loader: () => import('./Places.js').then(ex => ex.default),
  loading
});

export default AsyncPlaces;
