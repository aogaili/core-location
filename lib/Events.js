import React, { Component } from 'react';

import { Col, Grid, PostSkeleton, Media } from 'meteor/meronex:components';

import NeighborhoodMenu from './components/NeighborhoodMenu';
import { getEvents } from './api/eventsMethods';
import PreviewMap from './components/PreviewMap';
import EventItem from './components/EventItem';

export default class Events extends Component {
  state = {
    events: undefined,
    mainCoordinates: { lat: '24.4684219', lng: '54.3402198' }, // abu dhabi
    previewLocation: undefined
  };
  static propTypes = {};
  static defaultProps = {};

  componentDidMount() {
    this.getEvents();
  }

  getEvents = () => {
    // call server method getEvents
    getEvents.call({}, (err, res) => {
      let result;
      if (err) {
        console.log(err);
      } else {
        this.setState({
          events: res
        });
      }
      console.log(res);
      return result;
    });
  };

  showLocation = event => {
    console.log(event);

    if (event && event.venue) {
      const previewLocation = {
        geometry: {}
      };
      previewLocation.geometry.location = {
        lat: event.venue.lat,
        lng: event.venue.lon
      };

      this.setState({
        previewLocation
      });
    }
  };

  getEventsHtml = () => {
    if (!this.state.events) {
      return this.getSkeletons();
    }

    return this.state.events.map(event => {
      return (
        <EventItem
          key={event.id}
          showLocation={this.showLocation}
          event={event}
        />
      );
    });
  };

  getSkeletons = (count = 10) => {
    const skeletons = [];
    for (let i = 0; i < count; i += 1) {
      skeletons.push(<PostSkeleton key={i} />);
    }
    return (
      <div key={'none'} style={count === 1 ? {} : { marginTop: '-10px' }}>
        {skeletons}
      </div>
    );
  };

  render() {
    return (
      <div>
        <Grid fluid className="mobile-stretch">
          <Col md={3}>
            <NeighborhoodMenu onSelect={this.updatePlaceType} page="events" />
          </Col>
          <Col md={6}>
            <Media.List
              className="media-list-stream news-feed"
              style={{ marginTop: '-10px' }}>
              {this.getEventsHtml()}
            </Media.List>
          </Col>
          <Col md={3}>
            <PreviewMap
              mainCoordinates={this.state.mainCoordinates}
              previewLocation={this.state.previewLocation}
            />
          </Col>
        </Grid>
      </div>
    );
  }
}
