import React, { Component } from 'react';
// import PropTypes from 'prop-types';

import { Col, Grid, Media, PostSkeleton } from 'meteor/meronex:components';

import NeighborhoodMenu from './components/NeighborhoodMenu';
import NewsSources from './components/NewsSources';

import $S from 'scriptjs';

export default class News extends Component {
  state = {};
  static propTypes = {};
  static defaultProps = {};

  componentDidMount() {
    $S(['//platform.twitter.com/widgets.js'], () => {
      // TODO Where does this twttr come from? Can't find it anywhere in the project
      twttr.widgets.load();
    });
  }

  getSkeletons = (count = 10) => {
    const skeletons = [];
    for (let i = 0; i < count; i += 1) {
      skeletons.push(<PostSkeleton key={i} />);
    }
    return (
      <div key={'none'} style={count === 1 ? {} : { marginTop: '-10px' }}>
        {skeletons}
      </div>
    );
  };

  render() {
    return (
      <div>
        <Grid fluid className="mobile-stretch">
          <Col md={3}>
            <NeighborhoodMenu onSelect={this.updatePlaceType} page="news" />
          </Col>
          <Col md={6}>
            <a
              className="twitter-timeline elevation-2dp"
              style={{
                border: 'none'
              }}
              href="https://twitter.com/ResidentsClub/lists/abu-dhabi1">
              <div style={{ color: 'black' }}>{this.getSkeletons()}</div>
            </a>
            <Media.List className="media-list-stream news-feed" />
          </Col>
          <Col md={3}>
            <NewsSources />
          </Col>
        </Grid>
      </div>
    );
  }
}
