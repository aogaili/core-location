import React, { Component } from 'react';
// import PropTypes from 'prop-types';

import { Lightbox, XMasonry, XBlock } from 'meteor/meronex:components';
import InfiniteScroll from 'react-infinite-scroller';

import { getPhotos } from './api/photosMethods';

export default class Photos extends Component {
  state = {
    photos: [],
    loading: false,
    maxIds: [''],
    lightbox: {
      index: 0,
      show: false
    }
  };
  static propTypes = {};
  static defaultProps = {};

  componentDidMount() {}

  showLightbox = index => {
    this.setState({
      lightbox: {
        show: true,
        index
      }
    });
  };

  hideLightbox = () => {
    this.setState({
      lightbox: {
        show: false,
        index: 0
      }
    });
  };

  getPhotos = maxId => {
    // call server method get
    // if (this.state.maxId !== maxId) {
    getPhotos.call(
      {
        maxId
      },
      (err, res) => {
        let result;
        if (err) {
          console.log(err);
        } else {
          result = res;
          const maxIds = this.state.maxIds;
          maxIds.push(res.maxId);
          this.setState({
            photos: this.state.photos.concat(res.photos),
            maxIds
          });
        }
        return result;
      }
    );
    // }
  };

  loadMorePhotos = () => {
    const maxIds = this.state.maxIds;
    this.getPhotos(maxIds[maxIds.length - 1]);
  };

  getPhotosHtml = () => {
    const photos = this.state.photos.map((photo, index) => {
      return (
        <XBlock key={index}>
          <div
            className="photo-card"
            onClick={() => {
              this.showLightbox(index);
            }}>
            <div className="photo-card-container">
              <img
                width="293px"
                height="293px"
                style={{ marginBottom: '10px' }}
                src={photo.url}
              />
              <div className="photo-card-overlay">
                <div className="photo-container-text">{photo.caption}</div>
              </div>
            </div>
          </div>
        </XBlock>
      );
    });

    return <XMasonry>{photos}</XMasonry>;
  };

  render() {
    const { lightbox, photos } = this.state;
    const photosHtml = this.getPhotosHtml();

    const loader = (
      <div style={{ textAlign: 'center', marginTop: '20px' }}>
        <img src="/img/loading/loading_apple.gif" style={{ width: '80px' }} />
      </div>
    );

    return (
      <div>
        <div
          className="profile-header text-center"
          style={{
            backgroundImage: 'url(/img/neighborhood/photos/AbuDhabiBg.jpg)',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            height: '314px'
          }}>
          <div className="container">
            <div className="container-inner">
              <h3
                className="profile-header-user"
                style={{
                  fontSize: '40px',
                  letterSpacing: '6px',
                  wordSpacing: '20px',
                  textTransform: 'uppercase'
                }}>
                Abu Dhabi
              </h3>
              <p
                className="profile-header-bio"
                style={{
                  position: 'relative',
                  top: '30px'
                }}>
                Photos of Abu Dhabi from Instragram
              </p>
            </div>
          </div>
        </div>
        <div className="container m-y-md" data-grid="images">
          <InfiniteScroll
            pageStart={0}
            loadMore={() => {
              this.loadMorePhotos();
            }}
            hasMore={true}
            loader={loader}>
            {photosHtml}
          </InfiniteScroll>
        </div>
        <Lightbox
          onClose={() => this.hideLightbox()}
          show={lightbox.show}
          images={photos.map(photo => {
            return {
              url: photo.url,
              caption: <div style={{ fontSize: '20px' }}>{photo.caption}</div>,
              title: (
                <div>
                  <img src={photo.author.profileUrl} width={'50px'} />
                  <div
                    style={{
                      marginRight: '10px',
                      marginLeft: '10px',
                      display: 'inline-block',
                      fontSize: '20px',
                      fontWeight: '600'
                    }}>
                    <a
                      href={`https://www.instagram.com/${
                        photo.author.username
                      }/`}
                      target="_blank"
                      style={{
                        color: 'white'
                      }}>
                      {photo.author.name}
                    </a>
                  </div>
                </div>
              )
            };
          })}
          photoIndex={lightbox.index}
        />
      </div>
    );
  }
}
