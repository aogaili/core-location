import React, { Component } from 'react';

import {
  Col,
  Grid,
  Media,
  Panel,
  PostSkeleton,
  Form,
  Select
} from 'meteor/meronex:components';
import InfiniteScroll from 'react-infinite-scroller';

import { getPlaces } from './api/placesMethods';

import NeighborhoodMenu from './components/NeighborhoodMenu';
import PreviewMap from './components/PreviewMap';
import PlaceItem from './components/PlaceItem';

export default class Neighborhood extends Component {
  state = {
    places: [],
    pagetoken: undefined,
    hasMore: true,
    type: 'restaurant',
    rankby: 'distance',
    mainCoordinates: { lat: '24.4684219', lng: '54.3402198' }, // abu dhabi
    previewLocation: undefined
  };
  static propTypes = {};
  static defaultProps = {};

  componentDidMount() {
    this.getPlaces({
      coordinates: this.state.mainCoordinates,
      type: this.state.type,
      rankby: this.state.rankby,
      pagetoken: this.state.pagetoken
    });
  }

  getSkeletons = (count = 10) => {
    let calculatedCount = count;
    if (this.state.places.length > 0) {
      calculatedCount = 1;
    }
    const skeletons = [];
    for (let i = 0; i < calculatedCount; i += 1) {
      skeletons.push(<PostSkeleton key={i} />);
    }
    return (
      <div
        key={'none'}
        style={calculatedCount === 1 ? {} : { marginTop: '-10px' }}>
        {skeletons}
      </div>
    );
  };

  updatePreviewPlace = place => {
    this.setState({
      previewLocation: place
    });
  };

  updatePlaceType = type => {
    this.setState({
      type,
      places: [],
      pagetoken: undefined
    });
    this.getPlaces({
      coordinates: this.state.mainCoordinates,
      rankby: this.state.rankby,
      type,
      pagetoken: null
    });
  };

  updateStore = newList => {
    const currentList = this.state.places.slice(0);
    let mergedList = currentList;
    newList.forEach(newPlace => {
      const duplicate = currentList.find(place => {
        return place.place_id === newPlace.place_id;
      });
      if (!duplicate) {
        mergedList = mergedList.concat(newPlace);
      }
    });
    return mergedList;
  };

  getPlaces = ({
    coordinates = this.state.mainCoordinates,
    rankby = this.state.rankby,
    type = this.state.type,
    pagetoken = null
  }) => {
    // call server method getPlaces
    getPlaces.call(
      {
        coordinates,
        rankby,
        type,
        pagetoken
      },
      (err, res) => {
        if (err) {
          console.log(err);
        } else if (res.nextPage !== this.state.pagetoken) {
          this.setState({
            places: this.updateStore(res.places),
            pagetoken: res.nextPage,
            hasMore: res.hasMore
          });
        }
        return res;
      }
    );
  };

  updateLocation = location => {
    // location: '24.4684219, 54.3402198' // alainTower
    // location: '45.491011,-73.583133' // leSeville
    // location: '45.416594, -75.705542' // 200bay
    // location: '51.371822, -0.264751' // ukHome
    // location '24.491162, 54.395152' // marinaBlue

    let coordinates;

    switch (location) {
      case 'alainTower':
        coordinates = { lat: '24.4684219', lng: '54.3402198' };
        this.setState({
          places: [],
          mainCoordinates: coordinates,
          previewLocation: undefined
        });
        break;
      case 'leSeville':
        coordinates = { lat: '45.491011', lng: '-73.583133' };
        this.setState({
          places: [],
          mainCoordinates: coordinates,
          previewLocation: undefined
        });
        break;
      case '200bay':
        coordinates = { lat: '45.416594', lng: '-75.705542' };
        this.setState({
          places: [],
          mainCoordinates: coordinates,
          previewLocation: undefined
        });
        break;
      case 'ukHome':
        coordinates = { lat: '51.371822', lng: '-0.264751' };
        this.setState({
          places: [],
          mainCoordinates: coordinates,
          previewLocation: undefined
        });
        break;
      case 'marinaBlue':
        coordinates = { lat: '24.491162', lng: '54.395152' };
        this.setState({
          places: [],
          mainCoordinates: coordinates,
          previewLocation: undefined
        });
        break;
      default:
    }
    this.getPlaces({ coordinates });
  };

  renderPlacesList = () => {
    let places = <div key={'empty'} />;
    if (this.state.places) {
      places = this.state.places.map(place => {
        return (
          <PlaceItem
            place={place}
            previewLocation={this.updatePreviewPlace}
            key={place.place_id}
          />
        );
      });
      return places;
    }
    return <div style={{ marginTop: '-10px' }}>{this.getSkeletons()}</div>;
  };

  render() {
    const places = this.renderPlacesList();

    const selectOptions = [
      { value: 'alainTower', label: 'Al Ain Tower' },
      { value: 'leSeville', label: 'Le Seville' },
      { value: '200bay', label: '200 Bay' },
      { value: 'ukHome', label: 'UK Home' },
      { value: 'marinaBlue', label: 'Marina Blue Building' }
    ];

    return (
      <div>
        <Grid fluid className="mobile-stretch">
          <Col md={3}>
            <NeighborhoodMenu
              showPlaces={true}
              onSelect={this.updatePlaceType}
              page={'places'}
            />
            <Panel>
              <Panel.Body>
                <Form
                  ref={form => {
                    this.context = form;
                  }}>
                  <Select
                    name="location"
                    label="Location"
                    options={selectOptions}
                    onChange={(a, b) => {
                      this.updateLocation(b);
                    }}
                  />
                </Form>
              </Panel.Body>
            </Panel>
          </Col>
          <Col md={6}>
            <Media.List className="media-list-stream news-feed">
              <InfiniteScroll
                pageStart={0}
                loadMore={() => {
                  this.getPlaces({
                    coordinates: this.state.mainCoordinates,
                    rankby: this.state.rankby,
                    type: this.state.type,
                    pagetoken: this.state.pagetoken
                  });
                }}
                hasMore={this.state.hasMore}
                loader={this.getSkeletons()}>
                {places}
              </InfiniteScroll>
            </Media.List>
          </Col>
          <Col md={3}>
            <PreviewMap
              mainCoordinates={this.state.mainCoordinates}
              previewLocation={this.state.previewLocation}
            />
          </Col>
        </Grid>
      </div>
    );
  }
}
