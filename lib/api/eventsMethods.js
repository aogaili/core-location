import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';

import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';

import { log, RemoteErr, settings } from 'meteor/meronex:base';

const getMeetupEventDetails = event => {
  return new Promise((resolve, reject) => {
    const originalEvent = event;

    const responseData = {};
    try {
      responseData.event_details = HTTP.call(
        'GET',
        `https://api.meetup.com/${originalEvent.group.urlname}/events/${
          originalEvent.id
        }/rsvps`,
        {
          params: {
            key: settings.MeetUp.key
          }
        }
      ).data;
      // console.log(event_details.data);

      const updatedEvent = Object.assign(originalEvent, responseData);
      updatedEvent.group.group_photo =
        responseData.event_details[0].group.group_photo;
      updatedEvent.group.members = responseData.event_details[0].group.members;
      updatedEvent.group.join_mode =
        responseData.event_details[0].group.join_mode;
      resolve(updatedEvent);
    } catch (e) {
      reject(e);
    }
  });
};

const getMeetupEvents = () => {
  let response;
  for (let trials = 3; trials >= 0; trials -= 1) {
    try {
      // location: '45.491011,-73.583133' // leSeville
      // location '24.491162, 54.395152' // marinaBlue
      // location: '51.371822, -0.264751' // ukHome
      // location: '45.416594, -75.705542' // 200bay

      response = HTTP.call('GET', 'https://api.meetup.com/2/open_events', {
        params: {
          lat: '24.491162',
          lon: '54.395152',
          key: '601d703810557f13757b56632801122',
          page: '30'
        }
      });
    } catch (e) {
      throw RemoteErr(
        'neighborhood.getEvents.fetchError',
        'exception fetching events data',
        e
      );
    }
    if (response.data) {
      break;
    }
  }
  return response;
};

const getFBToken = () => {
  const response = HTTP.call(
    'GET',
    'https://graph.facebook.com/oauth/access_token',
    {
      params: {
        client_id: Facebook.clientId,
        client_secret: Facebook.clientSecret,
        grant_type: Facebook.grantType
      }
    }
  );
  return response.data.access_token;
};

const getFBEvents = () => {
  const EventSearch = require('facebook-events-by-location-core');

  const es = new EventSearch();

  try {
    const accessToken = getFBToken();
    console.log(accessToken);
    es
      .search({
        lat: 24.491162,
        lng: 54.395152,
        distance: 2500,
        accessToken
      })
      .then(events => {
        console.log(JSON.stringify(events));
      })
      .catch(error => {
        console.error(JSON.stringify(error));
      });
  } catch (e) {
    console.log(e);
  }
};

export const getEvents = new ValidatedMethod({
  name: 'getEvents',
  validate: new SimpleSchema({}).validator(),
  async run() {
    let result;
    if (Meteor.isServer) {
      log.call('getEvents called');
      result = getMeetupEvents().data.results;
      log.result('getEvents returns ');
    }
    return result;
  }
});
