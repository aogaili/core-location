import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';

import { settings, log } from 'meteor/meronex:base';

const getTwitLists = () => {
  const Twit = require('twit');

  const T = new Twit({
    consumer_key: settings.Twitter.consumerKey,
    consumer_secret: settings.Twitter.consumerSecret,
    access_token: settings.Twitter.accessToken,
    access_token_secret: settings.Twitter.accessTokenSecret
  });

  T.get('/lists/list', (err, data, response) => {
    console.log(data);
  });
};

const getTwitListMembers = () => {
  return new Promise((resolve, reject) => {
    const Twit = require('twit');

    const T = new Twit({
      consumer_key: settings.Twitter.consumerKey,
      consumer_secret: settings.Twitter.consumerSecret,
      access_token: settings.Twitter.accessToken,
      access_token_secret: settings.Twitter.accessTokenSecret
    });

    //
    T.get(
      '/lists/members',
      { list_id: '915170336047542277', slug: 'abu-dhabi1', count: 5000 },
      (err, data, response) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      }
    );
  });
};

export const getNewsSources = new ValidatedMethod({
  name: 'getNewsSources',
  validate: new SimpleSchema({}).validator(),
  async run() {
    let result;
    if (Meteor.isServer) {
      log.call('getNewsSources called');
      const newsSources = await getTwitListMembers();
      result = newsSources.users;
      log.result('getNewsSources returns ');
    }
    return result;
  }
});
