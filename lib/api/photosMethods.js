import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';

import { log, settings } from 'meteor/meronex:base';

/**
 * This is currently based on hack
 * https://medium.com/@h4t0n/instagram-data-scraping-550c5f2fb6f1
 * and pagination:
 * https://stackoverflow.com/questions/49265013/instagram-a-1max-id-end-cursor-isnt-working-for-public-user-feeds/49266320#49266320
 * and https://github.com/postaddictme/instagram-php-scraper/issues/288
 * The issue is that instagram private API is limited and doesn't allow us to get a user public images
 * This might stop working anytime!
 *
 * This call should be cached with with cron job so the user doesn't wait for the data to be fetched
 */

const profileMap = new Map();

const getProfileInfo = profileId => {
  return new Promise((resolve, reject) => {
    log.call('profileId called');

    if (profileMap.has(profileId)) {
      return resolve(profileMap.get(profileId));
    }
    try {
      const response = HTTP.call(
        'GET',
        `https://www.instagram.com/${profileId}/?__a=1`
      );
      const profile = response.data.graphql.user;
      profileMap.set(profileId, profile);
      resolve(profile);
    } catch (e) {
      reject(e);
    }
  });
};

const getInstaPhotos = maxId => {
  if (Meteor.isServer) {
    return new Promise(async (resolve, reject) => {
      log.call('getInstaPhotos called');
      try {
        const user = await getProfileInfo('visitabudhabi');
        const response = HTTP.call(
          'GET',
          'https://instagram.com/graphql/query/?query_id=17888483320059182&id=311125313&first=12',
          {
            params: {
              after: maxId || ''
            }
          }
        );

        const result = {};
        const photos = [];

        response.data.data.user.edge_owner_to_timeline_media.edges.forEach(
          item => {
            photos.push({
              id: item.node.id,
              url: item.node.display_url,
              caption: item.node.edge_media_to_caption.edges[0].node.text,
              author: {
                name: user.full_name,
                username: user.username,
                profileUrl: user.profile_pic_url
              }
            });
          }
        );

        result.photos = photos;
        result.maxId =
          response.data.data.user.edge_owner_to_timeline_media.page_info.end_cursor;

        resolve(result);
      } catch (e) {
        reject(e);
      }
      log.result('getInstaPhotos returns ');
    });
  }
};

export const getPhotos = new ValidatedMethod({
  name: 'getPhotos',
  validate: new SimpleSchema({
    maxId: {
      type: String,
      optional: true
    }
  }).validator(),
  async run({ maxId }) {
    let result;
    if (Meteor.isServer) {
      log.call('getInstaPhotos called');
      const photos = await getInstaPhotos(maxId);
      if (photos) {
        result = photos;
      } else {
        result = [];
      }
      log.result('getPhotos returns ');
    }
    return result;
  }
});
