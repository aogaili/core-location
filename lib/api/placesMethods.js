import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';

import { log, RemoteErr, settings } from 'meteor/meronex:base';

import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import geolib from 'geolib';

export const getPlaces = new ValidatedMethod({
  name: 'getPlaces',
  validate: new SimpleSchema({
    coordinates: {
      type: Object,
      blackbox: true
    },
    rankby: {
      type: String
    },
    type: {
      type: String
    },
    pagetoken: {
      type: String,
      optional: true
    }
  }).validator(),
  async run({ coordinates, rankby, type, pagetoken = null }) {
    let result;
    if (Meteor.isServer) {
      log.call('getPlaces called');
      try {
        const response = HTTP.call(
          'GET',
          settings.GoogleMaps.placesSearchEndpoint,
          {
            params: {
              location: `${coordinates.lat}, ${coordinates.lng}`,
              rankby,
              type,
              pagetoken,
              key: settings.GoogleMaps.key
            }
          }
        );
        result = {};
        result.nextPage = response.data.next_page_token;
        if (result.nextPage === undefined) {
          result.nextPage = null;
          result.hasMore = false;
        } else {
          result.hasMore = true;
        }
        result.places = [];
        for (let i = 0; i < response.data.results.length; i += 1) {
          const newPlace = response.data.results[i];
          const distance = geolib.getDistance(
            { latitude: coordinates.lat, longitude: coordinates.lng },
            {
              latitude: newPlace.geometry.location.lat,
              longitude: newPlace.geometry.location.lng
            }
          );
          newPlace.distance = distance;
          result.places.push(newPlace);
        }
        log.result('getPlaces returns ');
        return result;
      } catch (e) {
        throw RemoteErr(
          'neighborhood.getPlaces.fetchError',
          'exception fetching places data',
          e
        );
      }
    }
    return result;
  }
});

export const getPlaceDetails = new ValidatedMethod({
  name: 'getPlaceDetails',
  validate: new SimpleSchema({
    placeid: {
      type: String
    }
  }).validator(),
  run({ placeid }) {
    let result;
    if (Meteor.isServer) {
      log.call('getPlaceDetails called');
      log.info(`placeid: ${placeid}`);
      let response;
      try {
        response = HTTP.call('GET', settings.GoogleMaps.placeDetailsEndpoint, {
          params: {
            placeid,
            key: settings.GoogleMaps.key
          }
        });
      } catch (e) {
        throw RemoteErr(
          'neighborhood.getPlaceDetails.fetchError',
          'exception fetching place details data',
          e
        );
      }
      result = response.data.result;
      log.result('getPlaceDetails returns ');
    }
    return result;
  }
});
