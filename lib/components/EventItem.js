import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Media } from 'meteor/meronex:components';

import AddToCalendar from 'react-add-to-calendar';
import moment from 'moment';

export default class EventItem extends Component {
  state = {};
  static propTypes = {
    event: PropTypes.shape({
      name: PropTypes.string,
      description: PropTypes.string,
      time: PropTypes.string,
      duration: PropTypes.string,
      event_url: PropTypes.string,
      venue: PropTypes.shape({
        name: PropTypes.string,
        address_1: PropTypes.string,
        city: PropTypes.string,
        country: PropTypes.string
      }),
      yes_rsvp_count: PropTypes.string
    }),
    showLocation: PropTypes.func,
    group: PropTypes.shape({
      who: PropTypes.string
    })
  };
  static defaultProps = {};

  componentDidMount() {}

  showLocation = event => this.props.showLocation(event);

  render() {
    const { event } = this.props;

    if (!event) {
      return null;
    }

    const eventDetails = {
      title: event.name,
      description: `Event Meetup Page: ${event.event_url} <br/> ${
        event.description
      }`,
      location: event.venue ? event.venue.name : event.event_url,
      startTime: new Date(event.time),
      endTime: event.duration
        ? new Date(event.time + event.duration)
        : new Date(event.time)
    };

    return (
      <Media.ListItem
        className="event-item list-group-item elevation-2dp post"
        onMouseEnter={() => {
          this.showLocation(event);
        }}>
        <Media.Body style={{ overflow: 'hidden' }}>
          <div className="event-title">{event.name}</div>
          <div className="event-time">
            {moment(event.time).format('dddd, MMM Do YYYY, h:mm A')}
            <span style={{ fontSize: '12px' }}>
              &nbsp;( {moment(event.time).fromNow()} )
            </span>
          </div>
          <div
            style={{
              textAlign: 'center',
              margin: '15px'
            }}>
            <AddToCalendar event={eventDetails} />
          </div>
          {event.venue && (
            <div>
              <div className="event-venue">
                {event.venue.name},{' '}
                {`${event.venue.address_1} ${
                  event.venue.city
                }, ${event.venue.country.toUpperCase()}`}
              </div>
            </div>
          )}
          {!event.venue && (
            <div className="event-venue-private">
              Location details are available to members only.
            </div>
          )}
          <div className="event-attendance">
            {event.yes_rsvp_count} {event.group.who} Attending
          </div>

          <div
            className={'event-description'}
            dangerouslySetInnerHTML={{ __html: event.description }}
          />
          <div className="event-meetup">
            <a
              href={event.event_url}
              target="_blank"
              className="event-meetup-link">
              <strong>Check out this Meetup →</strong>
            </a>
          </div>
        </Media.Body>
      </Media.ListItem>
    );
  }
}
