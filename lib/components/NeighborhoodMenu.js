import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { SmartAvatar } from 'meteor/meronex:components';

import { Link } from 'react-router';

export default class NeighborhoodMenu extends Component {
  state = {
    placeType: 'restaurant'
  };
  static propTypes = {
    onSelect: PropTypes.func,
    showPlaces: PropTypes.bool,
    page: PropTypes.string
  };
  static defaultProps = {};

  componentDidMount() {}

  getPageMenuStyle = name => {
    return name === this.props.page
      ? { color: 'rgb(101, 195, 232)' }
      : { color: 'rgba(0, 0, 0, 0.55)' };
  };

  getMenu = links => {
    const styleMenuIcon = {
      width: '35px',
      display: 'inline-block'
    };
    const styleImg = {
      width: '35px'
    };

    return links.map((item, index) => {
      return (
        <li key={index}>
          <div style={styleMenuIcon}>
            <img style={item.style ? item.style : styleImg} src={item.img} />
          </div>

          <span
            className="menu-title"
            style={{
              marginLeft: '15px',
              cursor: 'pointer',
              color:
                item.type === this.state.placeType
                  ? '#65C3E8'
                  : 'rgba(0, 0, 0, 0.55)'
            }}
            onClick={() => {
              this.setState({
                placeType: item.type
              });
              this.props.onSelect(item.type);
            }}>
            {item.name}
          </span>
        </li>
      );
    });
  };

  render() {
    const border = {
      backgroundColor: 'white',
      borderRadius: '100%',
      width: '103px',
      height: '102px',
      position: 'relative',
      top: '-70px',
      margin: '0 auto',
      marginBottom: '-60px',
      boxShadow: '0 1px 1px rgba(0, 0, 0, 0.1)'
    };

    const links = [
      {
        name: 'Restaurants',
        img: '/img/neighborhood/restaurants.png',
        type: 'restaurant'
      },
      {
        name: 'Cafes',
        img: '/img/neighborhood/cafe.png',
        type: 'cafe',
        style: { width: '40px' }
      },
      {
        name: 'Parks',
        img: '/img/neighborhood/parks.png',
        type: 'park',
        style: { width: '40px' }
      },
      {
        name: 'Libraries',
        img: '/img/neighborhood/libraries.png',
        type: 'library'
      },
      {
        name: 'Gyms',
        img: '/img/neighborhood/gyms.png',
        type: 'gym'
      },
      {
        name: 'Schools',
        img: '/img/neighborhood/school.png',
        type: 'school'
      },
      {
        name: 'Hospitals',
        img: '/img/neighborhood/hospitals.png',
        type: 'hospital'
      },
      {
        name: 'Pharmacies',
        img: '/img/neighborhood/pharmacy.png',
        type: 'pharmacy'
      },
      {
        name: 'Shopping Mall',
        img: '/img/neighborhood/shopping_mall.png',
        type: 'shopping_mall'
      },
      {
        name: 'Hair Care',
        img: '/img/neighborhood/hair_care.png',
        type: 'hair_care'
      },
      {
        name: 'Banks',
        img: '/img/neighborhood/banks.png',
        type: 'bank'
      },
      {
        name: 'Laundries',
        img: '/img/neighborhood/laundry.png',
        type: 'laundry'
      },
      {
        name: 'Car Rentals',
        img: '/img/neighborhood/car_rental.png',
        type: 'car_rental',
        style: {
          marginLeft: '-8px',
          width: '50px'
        }
      },
      {
        name: 'Car Dealers',
        img: '/img/neighborhood/car_dealer.png',
        type: 'car_dealer'
      },
      {
        name: 'Car Wash',
        img: '/img/neighborhood/car_wash.png',
        type: 'car_wash'
      },
      {
        name: 'Nightlife',
        img: '/img/neighborhood/nightlife.png',
        type: 'night_club'
      },
      {
        name: 'Convenience Store',
        img: '/img/neighborhood/convenience_store.png',
        type: 'convenience_store'
      }
    ];

    const menuItems = this.getMenu(links);

    const showPlaces = this.props.showPlaces;

    const styleIcon = {
      width: '60px'
    };

    return (
      <div className="panel panel-default panel-profile visible-md visible-lg">
        <div
          className="panel-heading"
          style={{
            backgroundImage: 'url(/img/neighborhood.jpg)'
          }}
        />
        <div className="panel-body text-center">
          <div style={border}>
            <SmartAvatar
              id="building-avatar"
              style={{ position: 'relative', top: '3px' }}
              loaderRadius={35}
              size={95}
              enableUpdate={true}
              src="/img/samples/seville1.jpg"
            />
          </div>
          <h5 className="panel-title">
            <a className="text-inherit" href="#">
              {this.props.page === 'places' && (
                <span>Neighborhood Places </span>
              )}
              {this.props.page === 'events' && (
                <span>Neighborhood Events </span>
              )}
              {this.props.page === 'news' && <span>Neighborhood News </span>}
            </a>
          </h5>

          <p className="m-b-md" style={{ marginTop: '15px' }}>
            know your neighborhood places, events and news
          </p>

          <div>
            <ul className="panel-menu">
              <li className="panel-menu-item">
                <Link to={'/neighborhood'}>
                  <div>
                    <img
                      src="https://cdn3.iconfinder.com/data/icons/minimal-utility/128/Map.png"
                      style={styleIcon}
                    />
                    <div
                      className={'page-menu-item'}
                      style={this.getPageMenuStyle('places')}>
                      Places
                    </div>
                  </div>
                </Link>
              </li>
              <li className="panel-menu-item">
                <Link to={'/events'}>
                  <div>
                    <img
                      src="https://cdn4.iconfinder.com/data/icons/flat-icon-set/2133/flat_icons-graficheria.it-06.png"
                      style={styleIcon}
                    />
                    <div
                      className={'page-menu-item'}
                      style={this.getPageMenuStyle('events')}>
                      Events
                    </div>
                  </div>
                </Link>
              </li>
            </ul>
            <ul className="panel-menu">
              <li className="panel-menu-item">
                <Link to={'/news'}>
                  <div>
                    <img
                      src="https://blog.agilebits.com/wp-content/uploads/2014/11/news-icon.png"
                      style={styleIcon}
                    />
                    <div
                      className={'page-menu-item'}
                      style={this.getPageMenuStyle('news')}>
                      News
                    </div>
                  </div>
                </Link>
              </li>
              <li className="panel-menu-item">
                <Link to={'/photos'}>
                  <div>
                    <img
                      src="http://www.myiconfinder.com/uploads/iconsets/1885a6e83769f02e9448d136e58d780f-pictures.png"
                      style={styleIcon}
                    />
                    <div className={'page-menu-item'}>Photos</div>
                  </div>
                </Link>
              </li>
            </ul>

            {showPlaces && (
              <div>
                <hr />
                <ul
                  className="list-unstyled list-spaced"
                  style={{
                    marginLeft: '10px',
                    marginTop: '5px',
                    fontSize: '16px',
                    textAlign: 'left'
                  }}>
                  {menuItems}
                </ul>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
