import React, { Component } from 'react';

import { Media } from 'meteor/meronex:components';

import { getNewsSources } from '../api/newsMethods';

export default class NewsSources extends Component {
  state = {
    sources: undefined,
    mounted: false
  };
  static propTypes = {};
  static defaultProps = {};

  componentDidMount() {
    this.getNewsSources();
    this.setState({
      mounted: true
    });
  }

  componentWillUnmount() {
    this.setState({
      mounted: false
    });
  }

  getNewsSources = () => {
    // call server method getNewsSources
    getNewsSources.call({}, (err, res) => {
      let result;
      if (err) {
        console.log(err);
      } else {
        result = res;
        if (this.state.mounted) {
          this.setState({
            sources: result
          });
        }

        console.log(res);
      }
      return result;
    });
  };

  getSourcesHtml = () => {
    if (typeof this.state.sources === 'undefined') {
      return <div>Loading...</div>;
    }
    return this.state.sources.map(source => {
      return (
        <Media key={source.id}>
          <Media.Left>
            <img
              width="60px"
              className="media-object img-circle"
              src={source.profile_image_url}
            />
          </Media.Left>
          <Media.Body>
            <a href={source.url} target={'_blank'}>
              <strong>{source.name}</strong>
            </a>
            <div
              style={{
                fontSize: '13px',
                marginTop: '10px',
                backgroundColor: 'rgba(247, 247, 247, 0.38)',
                padding: '5px'
              }}>
              {source.description}
            </div>
          </Media.Body>
        </Media>
      );
    });
  };

  render() {
    const sources = this.getSourcesHtml();
    return (
      <div className="panel panel-default m-b-md visible-md visible-lg">
        <div className="panel-body">
          <div style={{ marginBottom: '5px' }}>
            <h5 className="m-t-0 m-b-15" style={{ display: 'inline' }}>
              Media Sources
            </h5>
            {this.state.sources && (
              <div
                style={{
                  display: 'inline',
                  fontSize: '12px',
                  marginLeft: '10px'
                }}>
                ({this.state.sources.length} sources)
              </div>
            )}
          </div>
          <Media.List
            style={{
              border: 'none',
              padding: '5px',
              height: '500px',
              overflow: 'scroll'
            }}>
            {sources}
          </Media.List>
        </div>
      </div>
    );
  }
}
