import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  Row,
  Col,
  Media,
  ImagesGrid,
  AsyncLightbox
} from 'meteor/meronex:components';

import Rater from 'react-rater';
import AnimateHeight from 'react-animate-height';

import { getPlaceDetails } from '../api/placesMethods';

export default class PlaceItem extends Component {
  state = {
    lightbox: {
      show: false,
      index: 0
    },
    showDetails: false,
    placeDetails: undefined,
    loadingDetails: false,
    galleryLoaded: false
  };
  static propTypes = {
    place: PropTypes.shape({
      photos: PropTypes.arrayOf(
        PropTypes.shape({
          length: PropTypes.number,
          photo_reference: PropTypes.string
        })
      ),
      opening_hours: PropTypes.shape({
        open_now: PropTypes.bool
      }),
      vicinity: PropTypes.string,
      place_id: PropTypes.string
    }),
    previewLocation: PropTypes.func,
    index: PropTypes.number
  };
  static defaultProps = {};

  componentDidMount() {}

  componentWillUnmount() {
    if (this.timer) {
      clearTimeout(this.time);
    }
  }

  togglePlaceDetails = show => {
    const { place } = this.props;
    if (!show) {
      this.setState({
        showDetails: false
      });
    } else if (!this.state.placeDetails) {
      this.getPlaceDetails(place.place_id);
    } else {
      this.setState({
        showDetails: true
      });
    }
  };

  onGalleryLoad = () => {
    this.setState({
      galleryLoaded: true
    });
  };

  toggleLightbox = show => {
    this.setState({
      lightbox: {
        show
      }
    });
  };

  formatPlaceDetails = () => {
    const originalPlaceDetails = this.state.placeDetails;
    const placeDetails = {};

    if (!originalPlaceDetails) {
      return undefined;
    }

    placeDetails.phone = originalPlaceDetails.international_phone_number;
    placeDetails.openingHours = originalPlaceDetails.opening_hours
      ? originalPlaceDetails.opening_hours.weekday_text
      : [];
    placeDetails.photos = originalPlaceDetails.photos
      ? originalPlaceDetails.photos.map(photo => {
          return `https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyCaPrG87PS5HeffH7h7jyKXGR58TpQ0D5w&maxwidth=400&photoreference=${
            photo.photo_reference
          }`;
        })
      : [];

    placeDetails.mapURL = originalPlaceDetails.url;
    placeDetails.website = originalPlaceDetails.website;

    return placeDetails;
  };

  isToday = day => {
    const dayNum = new Date().getDay();
    let GMAdjustedNum = dayNum - 1;

    if (GMAdjustedNum < 0) {
      GMAdjustedNum = 6;
    }
    return GMAdjustedNum === day;
  };

  getOpeningHours = placeDetails => {
    if (!placeDetails) return null;
    if (placeDetails.openingHours.length === 0) {
      return (
        <span
          style={{
            fontStyle: 'italic',
            color: '#909090'
          }}>
          Unavailable
        </span>
      );
    }
    return placeDetails.openingHours.map((hour, index) => {
      return (
        <div
          key={index}
          style={this.isToday(index) ? { fontWeight: '600' } : {}}>
          {hour}
        </div>
      );
    });
  };

  getPlacePhotos = placeDetails => {
    if (
      !placeDetails ||
      !placeDetails.photos ||
      placeDetails.photos.length === 0
    ) {
      return <div />;
    }

    const loaderContainer = {
      width: '100%',
      height: '318px',
      marginRight: '5px',
      minHeight: '100px',
      backgroundColor: 'rgb(230, 232, 234)',
      padding: '20px',
      borderRadius: '4px',
      textAlign: 'center'
    };

    const loader = {
      fontSize: '12px',
      marginLeft: '-62px',
      marginTop: '80px'
    };
    const styleImgContainer = {
      maxHeight: '652px',
      overflow: 'hidden'
    };

    return (
      <div style={styleImgContainer}>
        {!this.state.galleryLoaded && (
          <div style={loaderContainer}>
            <span className="imgLoader" style={loader}>
              Loading...
            </span>
          </div>
        )}

        <ImagesGrid
          images={placeDetails.photos}
          rootStyle={{ cursor: 'pointer' }}
          width="100%"
          onImageSelect={(event, src, index) => {
            this.setState({
              lightbox: {
                show: true,
                index
              }
            });
          }}
          onLoad={this.onGalleryLoad}
        />
      </div>
    );
  };

  getPlaceDetails = placeid => {
    // call server method getPlaceDetails
    this.setState({
      loadingDetails: true
    });
    getPlaceDetails.call({ placeid }, (err, res) => {
      let result;
      if (err) {
        console.log(err);
        this.setState({
          showDetails: true,
          loadingDetails: false
        });
      } else {
        result = res;
        this.setState({
          placeDetails: res
        });
        setTimeout(() => {
          this.setState({
            showDetails: true,
            loadingDetails: false
          });
        }, 100);
      }
      return result;
    });
  };

  render() {
    const { place, previewLocation } = this.props;
    const { showDetails, loadingDetails, lightbox } = this.state;

    const placeDetails = this.formatPlaceDetails();
    const openingHours = this.getOpeningHours(placeDetails);
    const photos = this.getPlacePhotos(placeDetails);

    return (
      <Media
        className="list-group-item place-item elevation-2dp"
        onMouseEnter={() => {
          previewLocation(place);
        }}>
        <Row style={{ marginButton: '-15px' }}>
          <Col md={7}>
            <div>
              <h5>{place.name}</h5>
            </div>

            <div>
              {place.rating && (
                <span style={{ color: '#E7711B', marginRight: '5px' }}>
                  {place.rating ? place.rating.toFixed(1) : 0}
                </span>
              )}
              <Rater
                total={5}
                rating={place.rating ? place.rating : 0}
                interactive={false}
              />
            </div>
            <div>{place.vicinity}</div>
            <div>
              {place.opening_hours &&
                place.opening_hours.open_now && (
                  <div
                    style={{
                      fontSize: '12px',
                      color: 'rgb(66, 183, 42)',
                      display: 'inline-block',
                      marginRight: '10px'
                    }}>
                    Open Now
                  </div>
                )}
              <div
                style={{
                  color: 'gray',
                  fontSize: '12px',
                  whiteSpace: 'nowrap',
                  display: 'inline-block'
                }}>
                {(place.distance / 1000).toFixed(1)} km away
              </div>
            </div>
            <div
              style={{
                color: '#44a3d3',
                fontSize: '13px',
                cursor: 'pointer'
              }}
              onClick={() => {
                this.togglePlaceDetails(!showDetails);
              }}>
              {showDetails ? <span>Less</span> : <span>More</span>} Details
              {loadingDetails && (
                <span
                  style={{ marginLeft: '5px', marginRight: '5px' }}
                  className="fa fa-refresh fa-spin"
                />
              )}
            </div>
          </Col>
          <Col md={4}>
            {place.photos &&
              place.photos.length > 0 && (
                <img
                  width="180px"
                  height="125px"
                  style={{ borderRadius: '4px' }}
                  src={`https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyBVPhFTTAtYhrgnSzPO3Ok1Eh2DUose4YI&maxwidth=400&photoreference=${
                    place.photos[0].photo_reference
                  }`}
                />
              )}

            {!place.photos && (
              <img
                width="180px"
                height="125px"
                style={{ borderRadius: '4px' }}
                src={'/img/neighborhood/no-place-img.png'}
              />
            )}
          </Col>
        </Row>
        <AnimateHeight duration={500} height={showDetails ? 'auto' : '0'}>
          <div>
            {placeDetails && (
              <div>
                <Row>
                  <Col md={7}>
                    <div style={{ marginTop: '20px' }}>
                      <div style={{ marginBottom: '5px' }}>
                        <i
                          className="fa fa-phone"
                          aria-hidden="true"
                          style={{ marginRight: '15px' }}
                        />
                        {placeDetails.phone && (
                          <span>{placeDetails.phone}</span>
                        )}
                        {!placeDetails.phone && (
                          <span
                            style={{
                              fontStyle: 'italic',
                              color: '#909090'
                            }}>
                            Unavailable
                          </span>
                        )}
                      </div>
                      <div style={{ marginBottom: '5px' }}>
                        <i
                          className="fa fa-globe"
                          aria-hidden="true"
                          style={{ marginRight: '15px' }}
                        />

                        {placeDetails.website && (
                          <a href={placeDetails.website} target="_blank">
                            Website
                          </a>
                        )}

                        {!placeDetails.website && (
                          <span
                            style={{
                              fontStyle: 'italic',
                              color: '#909090'
                            }}>
                            Unavailable
                          </span>
                        )}
                      </div>
                      <div style={{ marginBottom: '5px' }}>
                        <i
                          className="fa fa-map-o"
                          aria-hidden="true"
                          style={{ marginRight: '10px' }}
                        />
                        <a href={placeDetails.mapURL} target="_blank">
                          Google Map Page
                        </a>
                      </div>
                    </div>
                  </Col>
                  <Col md={5}>
                    <div
                      style={{
                        marginTop: '20px',
                        fontSize: '12px'
                      }}>
                      {placeDetails.openingHours && (
                        <div>
                          <div>
                            <span
                              style={{ fontWeight: '600', fontSize: '13px' }}>
                              <i className="fa fa-clock-o" aria-hidden="true" />{' '}
                              Opening Hours:
                            </span>
                          </div>
                          <div>{openingHours}</div>
                        </div>
                      )}
                    </div>
                  </Col>
                </Row>
                {photos}
                <AsyncLightbox
                  onClose={() => this.toggleLightbox(false)}
                  show={lightbox.show}
                  images={placeDetails.photos.map(photo => {
                    return { url: photo };
                  })}
                  photoIndex={lightbox.index}
                />
              </div>
            )}
          </div>
        </AnimateHeight>
      </Media>
    );
  }
}
