import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Gmaps, Marker } from 'react-gmaps';

const params = { v: '3.exp', key: 'AIzaSyCaPrG87PS5HeffH7h7jyKXGR58TpQ0D5w' };

export default class PreviewMap extends Component {
  state = {
    placeDetails: undefined
  };
  static propTypes = {
    mainCoordinates: PropTypes.object,
    previewLocation: PropTypes.object
  };
  static defaultProps = {};

  componentDidMount() {}

  getZoom = () => {
    const { previewLocation } = this.props;
    if (!previewLocation) {
      return 17;
    }
    const distance = previewLocation.distance;

    if (distance < 150) {
      return 17;
    }
    if (distance < 300) {
      return 16;
    }
    if (distance < 400) {
      return 15;
    }
    if (distance < 1000) {
      return 14;
    }
    if (distance < 2000) {
      return 13;
    }
    if (distance < 4000) {
      return 12;
    }
    return 11;
  };

  render() {
    const { mainCoordinates, previewLocation } = this.props;

    let previewCoordinates = mainCoordinates;

    if (previewLocation) {
      previewCoordinates = previewLocation.geometry.location;
    }

    const zoom = this.getZoom();
    return (
      <div>
        <div
          style={{
            width: '280px',
            position: 'fixed',
            padding: '0px',
            border: '1px solid #d3e0e9'
          }}
          className="elevation-2dp">
          <Gmaps
            width={'100%'}
            height={'500px'}
            lat={previewCoordinates.lat}
            lng={previewCoordinates.lng}
            zoom={zoom}
            ref={instance => {
              this.Gmaps = instance;
            }}
            loadingMessage={'Loading Preview Map'}
            params={params}
            onMapCreated={this.onMapCreated}>
            <Marker
              lat={mainCoordinates.lat}
              lng={mainCoordinates.lng}
              icon={'/img/home_icon_marker.png'}
            />
            {previewCoordinates && (
              <Marker
                lat={previewCoordinates.lat}
                lng={previewCoordinates.lng}
                icon={'/img/place_map_marker.png'}
              />
            )}
          </Gmaps>
        </div>
      </div>
    );
  }
}
