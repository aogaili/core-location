Package.describe({
  name: 'meronex:location',
  summary:
    'Community location data aggregation services such as fetching nearby photos, events, news and places ',
  version: '1.0.0'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0');

  api.use('meronex:base');
  api.use('meronex:components');

  api.use('ecmascript');
  api.use('alanning:roles');
  api.use('less@2.7.11-rc.3');

  Npm.depends({
    twit: '2.2.9',
    'react-add-to-calendar': '0.1.5',
    geolib: '2.0.24'
  });
  api.addFiles('lib/stylesheets/location.less', 'client');

  api.mainModule('clientIndex.js', 'client');
  api.mainModule('serverIndex.js', 'server');
});
