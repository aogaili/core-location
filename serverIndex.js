export * from './lib/api/eventsMethods';
export * from './lib/api/newsMethods';
export * from './lib/api/photosMethods';
export * from './lib/api/placesMethods';
